module DrawingArea exposing
    ( Model, init
    , Msg, update
    , zoomIn, zoomOut
    , loadImage
    , createRectangle, createOutline, delete, select
    , view, selectHtml
    )


import Html as H
import Html.App as App
import Html.Events as HE
import Html.Attributes as HA
import Svg
import Svg.Attributes as SvgA
import Svg.Events as SvgE
import Dict
import Json.Decode as Json
import String

import RectangleSelection as RS
import OutlineSelection as OS
import Helpers as HP

main =
    App.program
        { init = init
        , update = update
        , subscriptions = (\model -> Sub.none)
        , view = view
        }


-- MODEL

type SelectionModel
    = RSModel RS.Model
    | OSModel OS.Model

type alias Model =
    { selections : Dict.Dict Int SelectionModel
    , currentSelection : Maybe Int
    , uid : Int
    , zoomLevel : Float
    , origin : (Float, Float)
    , mouseDown : Bool
    , downPos : Maybe (Int, Int)
    , imageUrl : Maybe String
    , imageLoaded : Bool
    }


init : (Model, Cmd Msg)
init =
    ( Model Dict.empty Nothing 0 1 (0,0) False Nothing Nothing False, Cmd.none )


-- UPDATE


type Msg
    -- Mouse Management
    = Down (Int, Int)
    | Move (Int, Int)
    | Up
    -- Area Management
    | Wheel Float
    | ZoomIn
    | ZoomOut
    -- Background Image Management
    | LoadImage
    | ImageLoaded
    -- Selection Management
    | CreateRectangle
    | CreateOutline
    | Delete
    | Select (Maybe Int)
    -- Selection Messages
    | Rectangle Int RS.Msg
    | Outline Int OS.Msg


-- Msg constructors from outside module
zoomIn = ZoomIn
zoomOut = ZoomOut
loadImage = LoadImage
createRectangle = CreateRectangle
createOutline = CreateOutline
delete = Delete
select = Select

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        -- Mouse Management
        Down (x,y) -> case model.currentSelection of
            Nothing -> (model, Cmd.none)
            Just id ->
                ( { model
                    | mouseDown = True
                    , downPos = Just (x,y)
                  }
                , downCmd model id (x,y)
                )
        Move (x',y') -> case model.currentSelection of
            Nothing -> (model, Cmd.none)
            Just id ->
                ( model
                , moveCmd model id (x',y')
                )
        Up -> case model.currentSelection of
            Nothing -> (model, Cmd.none)
            Just id ->
                ( { model
                    | mouseDown = False
                    , downPos = Nothing
                  }
                , Cmd.none
                )
        -- Area Management
        Wheel deltaY ->
            let
                zoomModifier = if deltaY > 0 then 0.5 else 2
            in
                ( { model | zoomLevel = zoomModifier * model.zoomLevel }
                , Cmd.none
                )
        ZoomIn ->
            ( { model | zoomLevel = 2 * model.zoomLevel }
            , Cmd.none
            )
        ZoomOut ->
            ( { model | zoomLevel = 0.5 * model.zoomLevel }
            , Cmd.none
            )
        -- Background Image Management
        LoadImage ->
            let
                url = "https://github.com/openMVG/ImageDataset_SceauxCastle/raw/master/images/100_7100.JPG"
            in
                ( { model | imageUrl = Just url }, Cmd.none )
        ImageLoaded ->
            ( { model | imageLoaded = True }, Cmd.none )
        -- Selection Management
        CreateRectangle ->
            newSelection model CreateRectangle
        CreateOutline ->
            newSelection model CreateOutline
        Delete ->
            ( { model
                | selections =
                    Dict.remove
                        (Maybe.withDefault model.uid model.currentSelection)
                        model.selections
                , currentSelection = Nothing
              }
            , Cmd.none
            )
        Select id ->
            ( { model
                | currentSelection =
                    case id of
                        Nothing -> Nothing
                        Just i -> if Dict.member i model.selections then Just i else Nothing
              }
            , Cmd.none
            )
        -- Selection Messages
        Rectangle id rMsg ->
            updateSelection model (Rectangle id rMsg)
        Outline id oMsg ->
            updateSelection model (Outline id oMsg)


downCmd : Model -> Int -> (Int, Int) -> Cmd Msg
downCmd model id (x,y) =
    let
        sModel = Dict.get id model.selections
    in
        case sModel of
            Just (RSModel m) ->
                Cmd.map (Rectangle id) <| HP.msgToCmd <| RS.changeGeometry (x,y) (0,0)
            Just (OSModel m) ->
                Cmd.map (Outline id) <| HP.msgToCmd <| OS.resetWithPoint (x,y)
            Nothing -> Cmd.none


moveCmd : Model -> Int -> (Int, Int) -> Cmd Msg
moveCmd model id (x',y') =
    let
        sModel = Dict.get id model.selections
    in
        case sModel of
            Just (RSModel m) ->
                let
                    (x,y) = Maybe.withDefault (0,0) model.downPos
                    left = min x x'
                    top = min y y'
                    width = abs (x-x')
                    height = abs (y-y')
                in
                    Cmd.map (Rectangle id) <| HP.msgToCmd <| RS.changeGeometry (left,top) (width,height)
            Just (OSModel m) ->
                Cmd.map (Outline id) <| HP.msgToCmd <| OS.addPoint (x',y')
            Nothing -> Cmd.none


newSelection : Model -> Msg -> (Model, Cmd Msg)
newSelection model msg =
    case msg of
        CreateRectangle ->
            let
                (rModel, rCmd) = RS.init 0 0 0 0
            in
                ( { model
                    | selections = Dict.insert model.uid (RSModel rModel) model.selections
                    , currentSelection = Just model.uid
                    , uid = model.uid + 1
                  }
                , Cmd.map (Rectangle <| Maybe.withDefault model.uid model.currentSelection) rCmd
                )
        CreateOutline ->
            let
                (oModel, oCmd) = OS.init
            in
                ( { model
                    | selections = Dict.insert model.uid (OSModel oModel) model.selections
                    , currentSelection = Just model.uid
                    , uid = model.uid + 1
                  }
                , Cmd.map (Outline <| Maybe.withDefault model.uid model.currentSelection) oCmd
                )
        _ -> (model, Cmd.none)

updateSelection : Model -> Msg -> (Model, Cmd Msg)
updateSelection model msg =
    case msg of
        Rectangle id rMsg ->
            let
                sModel = Dict.get id model.selections
                rOldModel = case sModel of
                    Just (RSModel r) -> r
                    _ -> RS.defaultModel
                (rModel, rCmd) = RS.update rMsg rOldModel
            in
                ( { model
                    | selections = Dict.update id (\_ -> Just <| RSModel rModel) model.selections
                  }
                , Cmd.map (Rectangle id) rCmd
                )
        Outline id oMsg ->
            let
                sModel = Dict.get id model.selections
                oOldModel = case sModel of
                    Just (OSModel o) -> o
                    _ -> OS.defaultModel
                (oModel, oCmd) = OS.update oMsg oOldModel
            in
                ( { model
                    | selections = Dict.update id (\_ -> Just <| OSModel oModel) model.selections
                  }
                , Cmd.map (Outline id) oCmd
                )
        _ -> (model, Cmd.none)



-- VIEW


view : Model -> H.Html Msg
view model =
    H.div
        [ HE.onMouseUp Up
        , onWheel Wheel
        ]
        [ H.p [] [ H.text ("Just text before the drawing area") ]
        , Svg.svg
            ( (svgTransform model.zoomLevel model.origin)
              :: [drawingAreaStyle]
              ++ offsetsEvents model.mouseDown
            )
            ( (drawImage model.imageUrl model.imageLoaded)
              :: (viewSelections model.selections)
            )
        , H.br [] []
        , H.p [] [ H.text (toString model) ]
        ]


svgTransform : Float -> (Float, Float) -> Svg.Attribute msg
svgTransform zoomLevel (x,y) =
    SvgA.transform <|
        "scale(" ++ toString zoomLevel ++ ") " ++
        "translate" ++ toString (-x,-y)

drawImage : Maybe String -> Bool -> Svg.Svg Msg
drawImage imageUrl imageLoaded = case imageUrl of
    Nothing ->
        Svg.image [] []
    Just url ->
        let visibility = case imageLoaded of
            True -> SvgA.visibility "visible"
            False -> SvgA.visibility "hidden"
        in
            Svg.image
                [ SvgA.xlinkHref url, SvgA.width "800", SvgA.height "400"
                , visibility
                , SvgE.onLoad ImageLoaded
                ]
                []

viewSelections : Dict.Dict Int SelectionModel -> List (Svg.Svg Msg)
viewSelections selections =
    List.map viewSelection (Dict.toList selections)

viewSelection : (Int, SelectionModel) -> Svg.Svg Msg
viewSelection (id, sModel) = case sModel of
    RSModel rModel ->
        App.map (Rectangle id) <| RS.view rModel
    OSModel oModel ->
        App.map (Outline id) <| OS.view oModel



selectHtml : Model -> H.Html Msg
selectHtml model =
    H.select
        [onChange <| Select << Result.toMaybe << String.toInt]
        ((H.option
            [HA.value "none", HA.selected (model.currentSelection == Nothing)]
            [H.text "None"])
        ::
        (List.map (optionTag model.currentSelection) <| Dict.toList model.selections))

onChange : (String -> msg) -> H.Attribute msg
onChange tagger =
    HE.on "change" (Json.map tagger HE.targetValue)

optionTag : Maybe Int -> (Int, SelectionModel) -> H.Html msg
optionTag currentId (id, selection) =
    H.option
        [ HA.value (toString id), HA.selected (currentId == Just id)]
        [ H.text <| toString id ++
            case selection of
                RSModel _ -> ": Rectangle"
                OSModel _ -> ": Outline"
        ]



{-| Get the wheel deltaY attribute of a mouse event -}
onWheel : ((Float -> msg) -> H.Attribute msg)
onWheel =
    HP.specialOn "wheel" deltaYDecoder identity

deltaYDecoder : Json.Decoder Float
deltaYDecoder = Json.at ["deltaY"] Json.float


drawingAreaStyle : H.Attribute msg
drawingAreaStyle =
    HA.style
        [ ("display", "inline-block")
        , ("width", "800px")
        , ("height", "400px")
        , ("border", "1px solid")
        ]

offsetsEvents : Bool -> List (H.Attribute Msg)
offsetsEvents down =
    let
        baseOffsets = [(HP.offsetOn "mousedown") Down]
    in
        case down of
            False -> baseOffsets
            True -> (HP.offsetOn "mousemove") Move :: baseOffsets
