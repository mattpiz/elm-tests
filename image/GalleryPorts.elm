port module GalleryPorts exposing (..)

-- load the image at the given url
port loadImage : String -> Cmd msg
port blobedImage : ((String, String, Int, Int) -> msg) -> Sub msg
