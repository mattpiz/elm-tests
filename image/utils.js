// Create an image blob from url and send it back through a port
function fetchAndSendImage(url, sendPort) {
	request = new XMLHttpRequest();
	request.onload = function(){
		newUrl = window.URL.createObjectURL(request.response);
		console.log(newUrl);
		var img = document.createElement("img");
		img.onload = function(){
			var w = img.naturalWidth;
			var h = img.naturalHeight;
			sendPort.send([url, newUrl, w, h]);
		};
		img.src = newUrl;
	};
	request.open("GET", url);
	request.responseType = "blob";
	request.send(null);
}
