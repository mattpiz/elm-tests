import Html as H
import Html.App as App
import Html.Events as HE
import Html.Attributes as HA
import Dict

import GalleryPorts
import ImageCollection
import Image
import Helpers as HP

main =
    App.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }




-- MODEL #############################################################




type alias Model =
    { collection : ImageCollection.Model
    }


init : (Model, Cmd Msg)
init =
    let
        (imColl, imCollMsg) = ImageCollection.init <| Just (420, 240)
    in
        ( Model imColl
        , Cmd.map ImColl imCollMsg
        )




-- UPDATE ############################################################




type Msg
    = AddImageByUrl String
    | ImageLoaded (String, String, Int, Int)
    | ImColl ImageCollection.Msg


update : Msg -> Model -> (Model, Cmd Msg)
update msg model = case msg of
    AddImageByUrl url ->
        ( model
        , GalleryPorts.loadImage url
        )
    ImageLoaded (url, blobUrl, width, height) ->
        let
            (im, imMsg) = Image.init blobUrl width height
        in
            ( model
            , Cmd.map ImColl <| HP.msgToCmd <| ImageCollection.Add url im
            )
    ImColl imCollMsg ->
        let
            (imColl, imCollMsg') =
                ImageCollection.update imCollMsg model.collection
        in
            ( Model imColl
            , Cmd.map ImColl imCollMsg'
            )



-- SUBSCRIPTIONS #####################################################


subscriptions : Model -> Sub Msg
subscriptions model =
    GalleryPorts.blobedImage ImageLoaded


-- VIEW ##############################################################


view : Model -> H.Html Msg
view model =
    let
        url1 = "img/house.jpg"
        url2 = "img/sceaux.jpg"
    in
        H.div []
            [ H.button
                [HE.onClick <| AddImageByUrl url1]
                [H.text "Load Image 1"]
            , H.button
                [HE.onClick <| AddImageByUrl url2]
                [H.text "Load Image 2"]
            , H.br [] []
            , App.map ImColl <| ImageCollection.defaultView model.collection
            ]
