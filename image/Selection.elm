module Selection exposing
    ( Style, defaultStyle, changeStyle, styleAttribute )

import Svg
import Svg.Attributes as SvgA
import VirtualDom


-- MODEL

type alias Style =
    { color : String
    , strokeWidth : Int
    , highlighted : Bool
    }

defaultStyle : Style
defaultStyle =
    Style "red" 1 False


-- UPDATE

changeStyle : Style -> Maybe String -> Maybe Int -> Maybe Bool
    -> (String -> Int -> Bool -> msg) -> msg
changeStyle style color strokeWidth highlighted msgConstructor=
    case color of
        Nothing -> changeStyle style (Just style.color) strokeWidth highlighted msgConstructor
        Just c -> case strokeWidth of
            Nothing -> changeStyle style (Just c) (Just style.strokeWidth) highlighted msgConstructor
            Just s -> case highlighted of
                Nothing -> msgConstructor c s style.highlighted
                Just h -> msgConstructor c s h


-- VIEW

styleAttribute : String -> Int -> Bool -> List (Svg.Attribute msg)
styleAttribute color strokeWidth highlighted =
    let
        opacityAttr = if highlighted then "0.5" else "0"
    in
        [ SvgA.stroke color
        , SvgA.fill color
        , SvgA.strokeWidth (toString strokeWidth)
        , SvgA.fillOpacity opacityAttr
        ]
