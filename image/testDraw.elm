import Html as H
import Html.App as App
import Html.Events as HE
import Html.Attributes as HA
import Svg

import Helpers as HP


main =
    App.program
        { init = init
        , update = update
        , subscriptions = (\model -> Sub.none)
        , view = view
        }


-- MODEL


type alias Model =
  { path : List (Int, Int)
  , down : Bool
  }


init : (Model, Cmd Msg)
init =
    (Model [] False, Cmd.none)


-- UPDATE


type Msg
    = Down (Int, Int)
    | Move (Int, Int)
    | Up


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Down (x,y) -> (Model [(x,y)] True, Cmd.none)
        Move (x,y) ->
            ( {model | path = List.append model.path [(x,y)]}
            , Cmd.none
            )
        Up -> (Model [] False, Cmd.none)


stop : Msg
stop = Up

-- VIEW


view : Model -> H.Html Msg
view model =
    H.body [ HE.onMouseUp Up ]
    [ Svg.svg ([drawingAreaStyle] ++ offsetsEvents model.down) []
    , H.br [] []
    , H.p [] [ H.text (toString model.path) ]
    ]


drawingAreaStyle : H.Attribute msg
drawingAreaStyle =
  HA.style
    [ ("display", "inline-block")
    , ("width", "800px")
    , ("height", "400px")
    , ("border", "1px solid")
    ]

offsetsEvents : Bool -> List (H.Attribute Msg)
offsetsEvents down =
    let
        baseOffsets = [(HP.offsetOn "mousedown") Down, HE.onMouseUp Up]
        --baseOffsets = [(HP.computedOffsetOn "mousedown") Down, HE.onMouseUp Up]
    in
        case down of
            False -> baseOffsets
            True -> (HP.offsetOn "mousemove" Move) :: baseOffsets
            --True -> (HP.computedOffsetOn "mousemove" Move) :: baseOffsets
