import Html as H
import Html.App as App
import Html.Events as HE
import Html.Attributes as HA
import Task

import DrawingArea

main =
    App.program
        { init = init
        , update = update
        , subscriptions = (\model -> Sub.none)
        , view = view
        }


-- MODEL


type alias Model =
  { drawingArea : DrawingArea.Model
  }


init : (Model, Cmd Msg)
init =
    let
        (drawModel, drawCmd) = DrawingArea.init
    in
        ( Model drawModel
        , Cmd.map Draw drawCmd )


-- UPDATE


type Msg
    -- Area Management
    = ZoomIn
    | ZoomOut
    -- Background Image Management
    | LoadImage
    -- Selection Management
    | CreateRectangle
    | CreateOutline
    | Delete
    | Select (Maybe Int)
    -- DrawingArea Messages
    | Draw DrawingArea.Msg


{-| Transform a message to a Cmd message -}
msgToCmd : msg -> Cmd msg
msgToCmd msg =
    Task.perform identity identity (Task.succeed msg)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        -- Area Management
        ZoomIn -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.zoomIn)
        ZoomOut -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.zoomOut)
        -- Background Image Management
        LoadImage -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.loadImage)
        -- Selection Management
        CreateRectangle -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.createRectangle)
        CreateOutline -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.createOutline)
        Delete -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.delete)
        Select id -> (model, Cmd.map Draw <| msgToCmd <| DrawingArea.select id)
        -- DrawingArea Messages
        Draw msg ->
            let
                (drawModel, drawCmd) = DrawingArea.update msg model.drawingArea
            in
                ( {model | drawingArea = drawModel}
                , Cmd.map Draw drawCmd
                )


-- VIEW


view : Model -> H.Html Msg
view model =
    H.body []
        [ App.map Draw <| DrawingArea.view model.drawingArea
        , H.button [HE.onClick ZoomIn] [H.text "Zoom in"]
        , H.button [HE.onClick ZoomOut] [H.text "Zoom out"]
        , H.button [HE.onClick LoadImage] [H.text "Load image"]
        , H.button [HE.onClick CreateRectangle] [H.text "Create Rectangle"]
        , H.button [HE.onClick CreateOutline] [H.text "Create Outline"]
        , H.button [HE.onClick Delete] [H.text "Delete"]
        , App.map Draw <| DrawingArea.selectHtml model.drawingArea
        ]
