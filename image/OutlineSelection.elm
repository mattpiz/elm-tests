module OutlineSelection exposing
    ( Model, init, defaultModel
    , Msg, update, changeStyle, addPoint, reset, resetWithPoint, triggerPointerEvents
    , view
    , subscriptions
    )

import Svg
import Svg.Attributes as SvgA
import Html.App as App
import String

import Selection as S


main =
    App.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


-- MODEL

type alias Model =
    { path : List Position
    , style : S.Style
    , pointerEvents : Bool
    }

type alias Position = (Int, Int)

init : (Model, Cmd Msg)
init =
    ( Model [] S.defaultStyle False
    , Cmd.none
    )

defaultModel : Model
defaultModel =
    let
        (model, cmd) = init
    in
        model


-- UPDATE

type Msg
    = Style String Int Bool
    | AddPoint Position
    | Reset
    | ResetWithPoint (Int, Int)
    | TriggerPointerEvents Bool


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Style color strokeWidth highlighted ->
            ( { model | style = S.Style color strokeWidth highlighted }
            , Cmd.none
            )
        AddPoint point ->
            ( { model | path = point :: model.path }, Cmd.none )
        Reset ->
            ( { model | path = [] }, Cmd.none )
        ResetWithPoint (x,y) ->
            ( { model | path = [(x,y)] }, Cmd.none )
        TriggerPointerEvents bool ->
            ( { model | pointerEvents = bool }, Cmd.none )


changeStyle : Model -> Maybe String -> Maybe Int -> Maybe Bool -> Msg
changeStyle model color strokeWidth highlighted =
    S.changeStyle model.style color strokeWidth highlighted Style


addPoint : (Int, Int) -> Msg
addPoint (x, y) =
    AddPoint (x, y)

reset : Msg
reset = Reset

resetWithPoint : (Int, Int) -> Msg
resetWithPoint (x, y) =
    ResetWithPoint (x, y)

triggerPointerEvents : Bool -> Msg
triggerPointerEvents bool = TriggerPointerEvents bool


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


-- VIEW

view : Model -> Svg.Svg msg
view model =
    Svg.polygon
        ( S.styleAttribute
            model.style.color
            model.style.strokeWidth
            model.style.highlighted
        ++
        [ SvgA.pointerEvents (if model.pointerEvents then "auto" else "none")
        , SvgA.points (pathToString model.path)
        ]) []


pathToString : List Position -> String
pathToString positions =
    String.join " " <| List.map posToString positions

posToString : Position -> String
posToString (x,y) = toString x ++ "," ++ toString y
