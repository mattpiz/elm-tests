module RectangleSelection exposing
    ( Model, init, defaultModel
    , Msg, update, changeStyle, changeGeometry, triggerPointerEvents
    , view
    , subscriptions
    )

import Svg
import Svg.Attributes as SvgA
import VirtualDom

import Selection as S


main =
    VirtualDom.programWithFlags
        { init = (\(left, top, width, height) -> init left top width height)
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


-- MODEL

type alias Model =
    { geometry :
        { pos : Position
        , size : Size
        }
    , style : S.Style
    , pointerEvents : Bool
    }

type alias Position =
    { x : Int
    , y : Int
    }

type alias Size =
    { width : Int
    , height : Int
    }


init : Int -> Int -> Int -> Int -> (Model, Cmd Msg)
init left top width height =
    ( Model { pos = Position left top, size = Size width height } S.defaultStyle False
    , Cmd.none
    )

defaultModel : Model
defaultModel =
    let
        (model, cmd) = init 0 0 0 0
    in
        model


-- UPDATE

type Msg
    = Style String Int Bool
    | Geometry Position Size
    | TriggerPointerEvents Bool


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Style color strokeWidth highlighted ->
            ( { model | style = S.Style color strokeWidth highlighted }
            , Cmd.none
            )
        Geometry pos size ->
            ( { model | geometry = { pos = pos, size = size } }
            , Cmd.none
            )
        TriggerPointerEvents bool ->
            ( { model | pointerEvents = bool }, Cmd.none )


changeStyle : Model -> Maybe String -> Maybe Int -> Maybe Bool -> Msg
changeStyle model color strokeWidth highlighted =
    S.changeStyle model.style color strokeWidth highlighted Style


changeGeometry : (Int, Int) -> (Int, Int) -> Msg
changeGeometry (x, y) (width, height) =
    Geometry (Position x y) (Size width height)

triggerPointerEvents : Bool -> Msg
triggerPointerEvents bool = TriggerPointerEvents bool


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


-- VIEW

view : Model -> Svg.Svg msg
view model =
    Svg.rect
        ( S.styleAttribute
            model.style.color
            model.style.strokeWidth
            model.style.highlighted
        ++
        [ SvgA.x (toString model.geometry.pos.x)
        , SvgA.y (toString model.geometry.pos.y)
        , SvgA.width (toString model.geometry.size.width)
        , SvgA.height (toString model.geometry.size.height)
        , SvgA.pointerEvents (if model.pointerEvents then "auto" else "none")
        ]) []
