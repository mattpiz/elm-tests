import Html as H
import Html.App as App
import Html.Attributes as HA
import Html.Events as HE


main =
    App.program
        { init = init
        , update = update
        , subscriptions = subs
        , view = view
        }




-- MODEL #############################################################




type alias Model =
    { int : Int
    , string : String
    }


init : (Model, Cmd Msg)
init =
    ( Model 0 ""
    , Cmd.none
    )




-- UPDATE ############################################################




type Msg
    = ChangeInt Int
    | ChangeString String


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        ChangeInt int ->
            ( {model | int = int}
            , Cmd.none
            )
        ChangeString string ->
            ( {model | string = string}
            , Cmd.none
            )




-- SUBS ##############################################################




subs : Model -> Sub Msg
subs model =
    Sub.none




-- VIEW ##############################################################




view : Model -> H.Html Msg
view model =
    H.div []
        [ H.p [] [H.text <| "int : " ++ toString model.int]
        , H.p [] [H.text <| "string : " ++ model.string]
        ]
