module Main exposing (..)

import Html as H exposing (Html)
import Html.App as App
import Html.Attributes as HA
import Html.Events as HE
import Svg exposing (Svg)
import Svg.Events as SvgE
import DOM
import Json.Decode as JD


main =
    App.program
        { init = init
        , update = update
        , subscriptions = subs
        , view = view
        }



-- MODEL #############################################################


type alias Model =
    { pos : ( Float, Float )
    , size : ( Float, Float )
    , counter : Int
    }


init : ( Model, Cmd Msg )
init =
    ( Model ( 0, 0 ) ( 0, 0 ) 0
    , Cmd.none
    )



-- UPDATE ############################################################


type Msg
    = Geometry ( Float, Float, Float, Float )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Geometry ( left, top, width, height ) ->
            ( { model | pos = ( left, top ), size = ( width, height ) }
            , Cmd.none
            )



-- SUBS ##############################################################


subs : Model -> Sub Msg
subs model =
    Sub.none



-- VIEW ##############################################################


view : Model -> H.Html Msg
view model =
    H.div []
        [ H.text "Text before."
        , H.br [] []
        , H.div
            [ HA.style
                [ ( "width", "800px" )
                , ( "height", "400px" )
                , ( "border", "1px solid black" )
                , ( "margin", "auto" )
                , ( "display", "block" )
                ]
            , HE.on "touchmove" <|
                JD.object2
                    (\( x, y ) rect ->
                        Geometry ( x - rect.left, y - rect.top, rect.width, rect.height )
                    )
                    (JD.at [ "changedTouches", "0" ] DOM.client)
                    (DOM.currentTarget <|
                        JD.oneOf
                            [ DOM.boundingClientRect
                              -- , JD.succeed <| DOM.Rectangle 0 1 2 3
                            ]
                    )
            ]
            [ Svg.svg
                [ HA.style
                    [ ( "width", "100%" )
                    , ( "height", "100%" )
                    , ( "background-color", "grey" )
                    , ( "display", "block" )
                    ]
                ]
                []
            ]
        , H.br [] []
        , H.text <| toString model
        ]
