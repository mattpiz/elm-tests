import Html as H
import Html.App as App
import Html.Events as HE
import Html.Attributes as HA
import Json.Decode as J

import DOM
import Helpers as HP


main =
    App.program
        { init = init
        , update = update
        , subscriptions = (\model -> Sub.none)
        , view = view
        }


-- MODEL


type alias Model =
  { down : Bool
  , left : Int
  , top : Int
  }


init : (Model, Cmd Msg)
init =
    (Model False 0 0, Cmd.none)


-- UPDATE


type Msg
    = Up
    | Down (Int, Int)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Down (x,y) -> (Model True x y, Cmd.none)
        Up -> init


-- VIEW


view : Model -> H.Html Msg
view model =
    H.body []
    [ H.p [] [H.text (toString model)]
    , H.p (boundingRect model)
        [ H.div [bordered] [H.text "1"]
        , H.div [bordered] [H.text "2"]
        ]
    , H.p []
        [ H.div (bordered::(boundingRect model)) [H.text "3"]
        , H.div (bordered::(boundingRect model)) [H.text "4"]
        ]
    , H.p [] [H.text (toString model)]
    ]


bordered : H.Attribute msg
bordered =
  HA.style
    [ ("display", "inline-block")
    , ("width", "200px")
    , ("height", "100px")
    , ("border", "1px solid")
    ]

boundingRect : Model -> List (H.Attribute Msg)
boundingRect model =
    (HE.onMouseUp Up)::(HP.offsetOn "mousedown" Down)::
    --(HE.onMouseUp Up)::(HP.computedOffsetOn "mousedown" Down)::
        (if model.down then [HP.offsetOn "mousemove" Down] else [])
        --(if model.down then [HP.computedOffsetOn "mousemove" Down] else [])
