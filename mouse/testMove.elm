import Html as H
import Html.App as App
import Mouse as M
import Dict


main =
    App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


-- MODEL

type alias Model =
    { path : List M.Position
    , down : Bool
    , pos : Maybe M.Position
    , subs : Dict.Dict String (Sub Msg)
    }


init : (Model, Cmd Msg)
init =
    ( Model [] False Nothing (Dict.fromList
        [ ("down", M.downs Down)
        , ("up", M.ups Up)
        ])
    , Cmd.none
    )


-- UPDATE

type Msg
    = Down M.Position
    | Move M.Position
    | Up M.Position


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Down pos ->
            ( {model | down = True, pos = Just pos, subs = Dict.insert "move" (M.moves Move) model.subs}
            , Cmd.none
            )
        Up pos ->
            ( {model | down = False, pos = Just pos, subs = Dict.remove "move" model.subs}
            , Cmd.none
            )
        Move pos ->
            ( {model | pos = Just pos}
            , Cmd.none
            )


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch (Dict.values model.subs)


-- VIEW

view : Model -> H.Html Msg
view model =
    H.body []
        [ H.div [] [ H.text ("Down: " ++ toString model.down) ]
        , H.div [] [ H.text ("Up: " ++ toString (not model.down)) ]
        , H.div [] [ H.text ("Pos: " ++ stringPos model.pos) ]
        ]

stringPos : Maybe M.Position -> String
stringPos position =
    case position of
        Nothing -> "unknow"
        Just pos -> toString pos
