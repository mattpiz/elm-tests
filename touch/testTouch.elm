import Html as H
import Html.App as App
import Html.Attributes as HA
import Html.Events as HE
import Json.Decode as JD
import Json.Encode as JE
import Dict exposing (Dict)


import Touch exposing ( TouchEvent(..), Touch )
import SingleTouch exposing ( SingleTouch, onSingleTouch )
import MultiTouch exposing ( MultiTouch, onMultiTouch )


main =
    App.program
        { init = init
        , update = update
        , subscriptions = subs
        , view = view
        }




-- MODEL #############################################################




type alias Model =
    { singleTouch : Maybe SingleTouch
    , multiTouch : Maybe MultiTouch
    }


init : (Model, Cmd Msg)
init =
    ( Model Nothing Nothing
    , Cmd.none
    )




-- UPDATE ############################################################




type Msg
    = SingleTouchMsg TouchEvent Touch
    | MultiTouchMsg MultiTouch


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        SingleTouchMsg touchEvent touch ->
            ( {model | singleTouch = Just <| SingleTouch touchEvent touch}
            , Cmd.none
            )
        MultiTouchMsg multiTouch ->
            ( {model | multiTouch = Just multiTouch}
            , Cmd.none
            )




-- SUBS ##############################################################




subs : Model -> Sub Msg
subs model =
    Sub.none




-- VIEW ##############################################################




view : Model -> H.Html Msg
view model =
    H.div []
        [ H.img ([ HA.src "http://lorempixel.com/200/100/" ] ++ onAllTouch) []
        , H.img ([ HA.src "http://lorempixel.com/400/100/" ] ++ onAllMultiTouch) []
        , H.img ([ HA.src "http://lorempixel.com/400/100/" ] ++ onAllMultiTouch) []
        , H.img ([ HA.src "http://lorempixel.com/200/100/" ] ++ onAllTouch) []
        , H.br [] []
        , H.text <| toString model.singleTouch
        , H.br [] []
        , H.text <| toString model.multiTouch
        ]


onAllTouch : List (H.Attribute Msg)
onAllTouch =
    [ onSingleTouch TouchStart Touch.preventAndStop <| SingleTouchMsg TouchStart
    , onSingleTouch TouchMove Touch.preventAndStop <| SingleTouchMsg TouchMove
    , onSingleTouch TouchEnd Touch.preventAndStop <| SingleTouchMsg TouchEnd
    , onSingleTouch TouchCancel Touch.preventAndStop <| SingleTouchMsg TouchEnd
    ]


onAllMultiTouch : List (H.Attribute Msg)
onAllMultiTouch =
    [ onMultiTouch TouchStart Touch.preventAndStop MultiTouchMsg
    , onMultiTouch TouchMove Touch.preventAndStop MultiTouchMsg
    , onMultiTouch TouchEnd Touch.preventAndStop MultiTouchMsg
    , onMultiTouch TouchCancel Touch.preventAndStop MultiTouchMsg
    ]
