module SingleTouch exposing (..)


import Html as H
import Html.Events as HE
import Json.Decode as JD exposing ( (:=) )
import Dict exposing (Dict)


import Touch exposing ( TouchEvent(..), Touch )




-- MODEL #############################################################




type alias SingleTouch =
    { touchType : TouchEvent
    , touch : Touch
    }




-- EVENTS HANDLING ###################################################




onSingleTouch : TouchEvent -> HE.Options -> (Touch -> msg) -> H.Attribute msg
onSingleTouch touchEvent options msgMaker =
    HE.onWithOptions
        (case touchEvent of
            TouchStart -> "touchstart"
            TouchMove -> "touchmove"
            TouchEnd -> "touchend"
            TouchCancel -> "touchcancel"
        )
        options
        (JD.map msgMaker <| singleTouchEventDecoder touchEvent)


-- A touch event (touchstart, touchmove, ...) decoder for only 1 touch
singleTouchEventDecoder : TouchEvent -> JD.Decoder Touch
singleTouchEventDecoder touchEvent =
    JD.map snd <| JD.at ["changedTouches", "0"] <| Touch.touchDecoder
