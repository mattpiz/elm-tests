module MultiTouch exposing (..)


import Html as H
import Html.Events as HE
import Json.Decode as JD exposing ( (:=) )
import Dict exposing (Dict)


import Touch exposing ( TouchEvent(..), Touch )




-- MODEL #############################################################




type alias MultiTouch =
    { touchType : TouchEvent
    , touches : Dict Int Touch
    , targetTouches : Dict Int Touch
    , changedTouches : Dict Int Touch
    }




-- EVENTS HANDLING ###################################################




onMultiTouch : TouchEvent -> HE.Options -> (MultiTouch -> msg) -> H.Attribute msg
onMultiTouch touchEvent options msgMaker =
    HE.onWithOptions
        (case touchEvent of
            TouchStart -> "touchstart"
            TouchMove -> "touchmove"
            TouchEnd -> "touchend"
            TouchCancel -> "touchcancel"
        )
        options
        (JD.map msgMaker <| JD.customDecoder JD.value <| decodeMultiTouch touchEvent)


-- Decode a multitouch JS Value into a MultiTouch
decodeMultiTouch : TouchEvent -> JD.Value -> Result String MultiTouch
decodeMultiTouch touchEvent value =
    Result.map4 MultiTouch
        (Ok touchEvent)
        (decodeTouchList ["touches"] value)
        (decodeTouchList ["targetTouches"] value)
        (decodeTouchList ["changedTouches"] value)


-- Decode a TouchList JS Value
decodeTouchList : List String -> JD.Value -> Result String (Dict Int Touch)
decodeTouchList touchListPath =
    JD.decodeValue
        (JD.at touchListPath touchListDecoder)


touchListDecoder : JD.Decoder (Dict Int Touch)
touchListDecoder =
    JD.maybe Touch.touchDecoder
        |> JD.dict
        |> JD.map ( Dict.values >> (List.filterMap identity) >> Dict.fromList )
