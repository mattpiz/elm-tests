module Touch exposing (..)


import Html as H
import Html.Events as HE
import Json.Decode as JD exposing ((:=))
import Dict exposing (Dict)




-- MODEL #############################################################




type TouchEvent
    = TouchStart
    | TouchMove
    | TouchEnd
    | TouchCancel


type alias Touch =
    { clientX : Float
    , clientY : Float
    }




-- EVENTS HANDLING ###################################################




-- Options to prevent default and stop propagation of an event
preventAndStop : HE.Options
preventAndStop =
    { stopPropagation = True
    , preventDefault = True
    }


-- A Touch decoder.
touchDecoder : JD.Decoder (Int, Touch)
touchDecoder =
    JD.object2 (,)
        ("identifier" := JD.int)
        ( JD.object2 Touch
            ("clientX" := JD.float)
            ("clientY" := JD.float)
        )
